﻿using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using System.Windows.Forms.Layout;
using System.Xml;

namespace CONTROLLER
{
    public enum eLanguage
    {
        TiengViet = 0,
        TiengAnh = 1
    }

    public class Language : IDisposable
    {
        private DataSet ds = new DataSet();
        public static int flag_language = (int)eLanguage.TiengViet;
        string from = "viet";
        string to = "anh";

        public Language()
        {
            ds.ReadXml("RESOURCE\\Language.xml");
        }

        public void SetLanguage(int val)
        {
            flag_language = val;
            switch (flag_language)
            {
                case (int)eLanguage.TiengViet:
                    from = "viet";
                    to = "anh";
                    break;
                case (int)eLanguage.TiengAnh:
                    from = "anh";
                    to = "viet";
                    break;
            }
        }

        private void ChangeMenu(ToolStripMenuItem item)
        {
            item.Text = this.get_text(ds.Tables[0], to, item.Text, from);
            for (int i = 0; i < item.DropDown.Items.Count; i++)
            {
                ToolStripItem subItem = item.DropDown.Items[i];
                if (item is ToolStripMenuItem)
                {
                    ChangeMenu(subItem as ToolStripMenuItem);
                }
            }
        }

        public void ChangeLanguage(Form frm)
        {
            
            frm.Text = this.get_text(this.ds.Tables[0], to, frm.Text, from);
            foreach (Control control in frm.Controls)
            {

                switch (control.GetType().ToString())
                {
                    case "System.Windows.Forms.Label":
                    case "System.Windows.Forms.Button":
                        control.Text = this.get_text(this.ds.Tables[0], to, control.Text, from);
                        break;
                    case "System.Windows.Forms.StatusStrip":
                        {
                            StatusStrip statusControl = control as StatusStrip;
                            for (int i = 0; i < statusControl.Items.Count; i++)
                            {
                                statusControl.Items[i].Text = get_text(ds.Tables[0], to, statusControl.Items[i].Text, from);
                            }
                            break;
                        }
                    case "System.Windows.Forms.MenuStrip":
                        {
                            MenuStrip menuControl = control as MenuStrip;
                            foreach (ToolStripMenuItem menu in menuControl.Items)
                            {
                                ChangeMenu(menu);
                            }
                            break;
                        }
                    case "System.Windows.Forms.ToolStrip":
                        {
                            ToolStrip toolControl = control as ToolStrip;
                            for (int i = 0; i < toolControl.Items.Count; i++)
                            {
                                toolControl.Items[i].Text = get_text(ds.Tables[0], to, toolControl.Items[i].Text, from);
                            }
                            break;
                        }

                    case "System.Windows.Forms.TabControl":
                        break;
                }
               
            }
        }

        private string get_text(DataTable dt, string to, string text, string from)
        {
            string str = text;
            string dkt = to + "='" + text + "'";
            DataRow dataRow = this.getrowbyid(dt, dkt);
            if (dataRow != null)
            {
                str = dataRow[from].ToString();
            }
            return str.Trim();
        }

        public DataRow getrowbyid(DataTable dt, string exp)
        {
            DataRow dataRow;
            try
            {
                dataRow = dt.Select(exp)[0];
            }
            catch
            {
                dataRow = null;
            }
            return dataRow;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resources
                ds.Dispose();
            }
            // free native resources
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}