﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CONTROLLER
{
    class CaptureScreen : IDisposable
    {
        PixelFormat pxF;

        public long imageQuality { get; set; }

        int bytesPerPixel;

        Rectangle screen;

        public Bitmap cur { get; private set; }

        public byte[] curBuff { get; private set; }

        public CaptureScreen()
        {
            pxF = PixelFormat.Format24bppRgb;
            imageQuality = 60;
            bytesPerPixel = Image.GetPixelFormatSize(pxF) / 8;

            screen = Screen.PrimaryScreen.Bounds;

            cur = new Bitmap(screen.Width, screen.Height, pxF);
            curBuff = new byte[screen.Width * screen.Height * bytesPerPixel];
        }

        public bool Capture()
        {
            try
            {
                using (Graphics g = Graphics.FromImage(cur))
                {
                    g.CopyFromScreen(0, 0, 0, 0, screen.Size, CopyPixelOperation.SourceCopy);
                    CompressBuff();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            
        }

        private void CompressBuff()
        {
            using (var ms = new MemoryStream())
            {
                JPEGCompress(cur, ms, imageQuality);
                curBuff = ms.ToArray();
            }
        }

        private int JPEGCompress(Image img, Stream str, long quality)
        {
            ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Jpeg);
            System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;
            EncoderParameters myEncoderParameters = new EncoderParameters(1);
            EncoderParameter ratio = new EncoderParameter(myEncoder, quality);
            myEncoderParameters.Param[0] = ratio;

            img.Save(str, jpgEncoder, myEncoderParameters);
            return (int)str.Length;
        }

        private ImageCodecInfo GetEncoder(ImageFormat jpeg)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == jpeg.Guid)
                {
                    return codec;
                }
            }
            return null;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resources
                cur.Dispose();
            }
            // free native resources
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
