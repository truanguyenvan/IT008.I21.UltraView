﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using CONTROLLER;
using UltraViewer.Properties;
using System.IO;
using System.Drawing.Imaging;

namespace UltraViewer
{
    public partial class formView : Form
    {
        Master myMaster;

        Language language;

        Point locationToolStripTool;

        Point prePos;
        Point curPos;
        

        int deltaWheel;
        bool Running;

        Bitmap slaveBitmap;
        Rectangle slaveScreen;

        delegate void CloseFormDelegate(Form form);
        public formView(ref Master _master)
        {
            InitializeComponent();
            this.Width = (int)(Screen.PrimaryScreen.Bounds.Width * 0.7);
            this.Height = (int)(Screen.PrimaryScreen.Bounds.Height * 0.7);

            myMaster = _master;

            language = new Language();

            prePos = new Point(0, 0);
            curPos = new Point(0, 0);

            deltaWheel = 0;
            Running = true;

            MouseWheel += formView_MouseWheel;

            Thread receiveThread = new Thread(Receiving);
            receiveThread.IsBackground = true;
            receiveThread.Start();
        }

        private void UpdateScreen()
        {
            pbxScreen.Image = slaveBitmap;
        }

        private void formView_Shown(object sender, EventArgs e)
        {
            if (Language.flag_language == (int)eLanguage.TiengAnh)
            {
                language.SetLanguage((int)eLanguage.TiengAnh);
                language.ChangeLanguage(this);
            }
            else
            {
                language.SetLanguage((int)eLanguage.TiengViet);
                language.ChangeLanguage(this);
            }
            tsMove.Left = tsTool.Left + tsTool.Size.Width - tsMove.Size.Width;

        }

        private void formView_Resize(object sender, EventArgs e)
        {
            tsMove.Left = tsTool.Left + tsTool.Size.Width - tsMove.Size.Width;
        }

        private void tslblMove_MouseDown(object sender, MouseEventArgs e)
        {
            locationToolStripTool = e.Location;
        }

        private void tslblMove_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                tsMove.Left = tsMove.Left + e.X - locationToolStripTool.X;
                tsTool.Left = tsTool.Left + e.X - locationToolStripTool.X;

                Cursor.Position = new Point(Cursor.Position.X,
                    this.Top + tsTool.Size.Height + tsMove.Top + tsMove.Height);
            }
        }

        private void tsbtnShowHide_Click(object sender, EventArgs e)
        {
            tsTool.Visible = !tsTool.Visible;
            if (!tsTool.Visible)
            {
                tsbtnShowHide.Image = Resources.down;
                tsMove.Location = new Point(tsMove.Location.X, 0);
            }
            else
            {
                tsbtnShowHide.Image = Resources.up;
                tsMove.Top = tsTool.Height;
            }

        }

        //Update slaver's screen
        private void Receiving()
        {
            System.Threading.Timer posTimer = new System.Threading.Timer(PosTimer_Tick);
            posTimer.Change(0, 50);

            System.Threading.Timer wheelTimer = new System.Threading.Timer(WheelTimer_Tick);
            wheelTimer.Change(0, 500);


            while (myMaster.ReceiveVarData() <= 0);
            using (var stream = new MemoryStream(myMaster.Data))
            {
                Image img = Image.FromStream(stream);
                slaveScreen.Size = img.Size;
            }

            while (myMaster.Connected && Running)
            {
                if (myMaster.ReceiveVarData() > 0)
                {
                    using (var stream = new MemoryStream(myMaster.Data))
                    {
                        Image img = Image.FromStream(stream);
                        slaveBitmap = new Bitmap(img);
                        UpdateScreen();
                    }
                }
            }

            posTimer.Dispose();
            wheelTimer.Dispose();

            if (Running)
            {
                //lost signal from remote host
                CloseForm(this);
            }
        }

        private void WheelTimer_Tick(object state)
        {
            if (deltaWheel != 0)
            {
                //Debug.WriteLine(deltaWheel.ToString(), "wheel timer");
                myMaster.SendVarData(DataConverter.FromMouseWheel(deltaWheel));
                deltaWheel = 0;
            }
            
        }

        private void PosTimer_Tick(object state)
        {
            if (!curPos.Equals(prePos))
            {
                prePos = curPos;
                myMaster.SendVarData(DataConverter.FromPositionMouse(curPos));
            }
        }


        //Control Events
        private void pbxScreen_MouseMove(object sender, MouseEventArgs e)
        {
            int wImage, hImage;
            float imageFactor = (float)slaveScreen.Width / slaveScreen.Height;
            if ((float)pbxScreen.Width / pbxScreen.Height < imageFactor)
            {
                wImage = pbxScreen.ClientSize.Width;
                hImage = (int)(wImage / imageFactor);
            }
            else
            {
                hImage = pbxScreen.ClientSize.Height;
                wImage = (int)(hImage * imageFactor);
            }

            int left = (pbxScreen.ClientSize.Width - wImage) / 2;
            int top = (pbxScreen.ClientSize.Height - hImage) / 2;


            int xLocation = (int)((e.Location.X - left) * slaveScreen.Width / wImage);
            int yLocation = (int)((e.Location.Y - top) * slaveScreen.Height / hImage);

            curPos = new Point(xLocation, yLocation);
        }

        private void pbxScreen_MouseDown(object sender, MouseEventArgs e)
        {
            SimWinInput.SimMouse.Action _action;
            
            switch (e.Button)
            {
                case MouseButtons.Left:
                    _action = SimWinInput.SimMouse.Action.LeftButtonDown;
                    break;
                case MouseButtons.Right:
                    _action = SimWinInput.SimMouse.Action.RightButtonDown;
                    break;
                case MouseButtons.Middle:
                    _action = SimWinInput.SimMouse.Action.MiddleButtonDown;
                    break;
                default:
                    _action = SimWinInput.SimMouse.Action.MoveOnly;
                    break;
            }

            MouseInfo _mouseInfo  = new MouseInfo(_action, e.Location.X, e.Location.Y);
            myMaster.SendVarData(DataConverter.FromMouse(_mouseInfo));
        }

        private void pbxScreen_MouseUp(object sender, MouseEventArgs e)
        {
            SimWinInput.SimMouse.Action _action;

            switch (e.Button)
            {
                case MouseButtons.Left:
                    _action = SimWinInput.SimMouse.Action.LeftButtonUp;
                    break;
                case MouseButtons.Right:
                    _action = SimWinInput.SimMouse.Action.RightButtonUp;
                    break;
                case MouseButtons.Middle:
                    _action = SimWinInput.SimMouse.Action.MiddleButtonUp;
                    break;
                default:
                    _action = SimWinInput.SimMouse.Action.MoveOnly;
                    break;
            }


            MouseInfo _mouseInfo = new MouseInfo(_action, e.Location.X, e.Location.Y);
            myMaster.SendVarData(DataConverter.FromMouse(_mouseInfo));
        }

        private void formView_MouseWheel(object sender, MouseEventArgs e)
        {
            deltaWheel += e.Delta;
            //Debug.WriteLine(deltaWheel.ToString(), "event mousewheel");
        }

        private void formView_KeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = true;

            KeyInfo _keyinfo = new KeyInfo(KeypressInfo.Down, (byte)e.KeyCode);
            myMaster.SendVarData(DataConverter.FromKey(_keyinfo));
        }

        private void formView_KeyUp(object sender, KeyEventArgs e)
        {
            e.Handled = true;

            KeyInfo _keyinfo = new KeyInfo(KeypressInfo.Up, (byte)e.KeyCode);
            myMaster.SendVarData(DataConverter.FromKey(_keyinfo));
        }

        private void tsbtnScreenshot_Click(object sender, EventArgs e)
        {
           // string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            try
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "JPeg Image|*.jpg|Bitmap Image|*.bmp|Gif Image|*.gif";
                saveFileDialog1.Title = "Save an Image File";
                saveFileDialog1.ShowDialog();
                if (saveFileDialog1.FileName != "")
                {
                    FileStream fs = (FileStream)saveFileDialog1.OpenFile();
                    switch (saveFileDialog1.FilterIndex)
                    {
                        case 1:
                            slaveBitmap.Save(fs, ImageFormat.Jpeg);
                            break;
                        case 2:
                            slaveBitmap.Save(fs, ImageFormat.Bmp);
                            break;
                        case 3:
                            slaveBitmap.Save(fs, ImageFormat.Gif);
                            break;
                    }
                    fs.Close();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Thao tác thất bại! Vui lòng thử lại.");
            }
        }

        private void tsbtnExit_Click(object sender, EventArgs e)
        {
            CloseForm(this);
        }

        private void formView_FormClosed(object sender, FormClosedEventArgs e)
        {
            Running = false;
        }

        private void CloseForm(Form form)
        {
            if (form.InvokeRequired)
            {
                CloseFormDelegate dlg = new CloseFormDelegate(CloseForm);
                form.Invoke(dlg, new object[] { form });
            }
            else
            {
                this.Close();
            }
        }

        private void tsbtnBlockControls_CheckedChanged(object sender, EventArgs e)
        {

            tsbtnBlockControls.Image = tsbtnBlockControls.Checked ? Resources.Unblock : Resources.Block;
            myMaster.SendVarData(DataConverter.FromStateBlockInput(tsbtnBlockControls.Checked));
        }
    }
}
