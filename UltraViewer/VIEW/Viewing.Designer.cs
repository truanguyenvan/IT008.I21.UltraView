﻿namespace UltraViewer
{
    partial class formView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();

                language.Dispose();

                slaveBitmap.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formView));
            this.tsTool = new System.Windows.Forms.ToolStrip();
            this.tsbtnScreenshot = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtnBlockControls = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbtnExit = new System.Windows.Forms.ToolStripButton();
            this.tsMove = new System.Windows.Forms.ToolStrip();
            this.tslblMove = new System.Windows.Forms.ToolStripLabel();
            this.tsbtnShowHide = new System.Windows.Forms.ToolStripButton();
            this.pbxScreen = new System.Windows.Forms.PictureBox();
            this.tsTool.SuspendLayout();
            this.tsMove.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxScreen)).BeginInit();
            this.SuspendLayout();
            // 
            // tsTool
            // 
            this.tsTool.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tsTool.Dock = System.Windows.Forms.DockStyle.None;
            this.tsTool.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsTool.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbtnScreenshot,
            this.toolStripSeparator1,
            this.tsbtnBlockControls,
            this.toolStripSeparator2,
            this.tsbtnExit});
            this.tsTool.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.tsTool.Location = new System.Drawing.Point(424, 0);
            this.tsTool.Name = "tsTool";
            this.tsTool.Size = new System.Drawing.Size(281, 23);
            this.tsTool.TabIndex = 2;
            this.tsTool.Text = "toolStrip1";
            // 
            // tsbtnScreenshot
            // 
            this.tsbtnScreenshot.Image = global::UltraViewer.Properties.Resources.screenshot;
            this.tsbtnScreenshot.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnScreenshot.Name = "tsbtnScreenshot";
            this.tsbtnScreenshot.Size = new System.Drawing.Size(110, 20);
            this.tsbtnScreenshot.Text = "Chụp màn hình";
            this.tsbtnScreenshot.Click += new System.EventHandler(this.tsbtnScreenshot_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 23);
            // 
            // tsbtnBlockControls
            // 
            this.tsbtnBlockControls.CheckOnClick = true;
            this.tsbtnBlockControls.Image = global::UltraViewer.Properties.Resources.Block;
            this.tsbtnBlockControls.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnBlockControls.Name = "tsbtnBlockControls";
            this.tsbtnBlockControls.Size = new System.Drawing.Size(100, 20);
            this.tsbtnBlockControls.Text = "Khóa thao tác";
            this.tsbtnBlockControls.CheckedChanged += new System.EventHandler(this.tsbtnBlockControls_CheckedChanged);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 23);
            // 
            // tsbtnExit
            // 
            this.tsbtnExit.Image = global::UltraViewer.Properties.Resources.exit;
            this.tsbtnExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnExit.Name = "tsbtnExit";
            this.tsbtnExit.Size = new System.Drawing.Size(58, 20);
            this.tsbtnExit.Text = "Thoát";
            this.tsbtnExit.Click += new System.EventHandler(this.tsbtnExit_Click);
            // 
            // tsMove
            // 
            this.tsMove.BackColor = System.Drawing.Color.White;
            this.tsMove.Dock = System.Windows.Forms.DockStyle.None;
            this.tsMove.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsMove.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslblMove,
            this.tsbtnShowHide});
            this.tsMove.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.tsMove.Location = new System.Drawing.Point(672, 23);
            this.tsMove.Name = "tsMove";
            this.tsMove.Size = new System.Drawing.Size(38, 20);
            this.tsMove.TabIndex = 3;
            this.tsMove.Text = "toolStrip2";
            // 
            // tslblMove
            // 
            this.tslblMove.AutoSize = false;
            this.tslblMove.BackgroundImage = global::UltraViewer.Properties.Resources.mover;
            this.tslblMove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.tslblMove.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tslblMove.Name = "tslblMove";
            this.tslblMove.Size = new System.Drawing.Size(20, 15);
            this.tslblMove.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tslblMove_MouseDown);
            this.tslblMove.MouseMove += new System.Windows.Forms.MouseEventHandler(this.tslblMove_MouseMove);
            // 
            // tsbtnShowHide
            // 
            this.tsbtnShowHide.AutoSize = false;
            this.tsbtnShowHide.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnShowHide.Image = global::UltraViewer.Properties.Resources.up;
            this.tsbtnShowHide.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnShowHide.Name = "tsbtnShowHide";
            this.tsbtnShowHide.Size = new System.Drawing.Size(17, 17);
            this.tsbtnShowHide.Text = "toolStripButton3";
            this.tsbtnShowHide.Click += new System.EventHandler(this.tsbtnShowHide_Click);
            // 
            // pbxScreen
            // 
            this.pbxScreen.BackColor = System.Drawing.Color.Black;
            this.pbxScreen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pbxScreen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbxScreen.Location = new System.Drawing.Point(0, 0);
            this.pbxScreen.Name = "pbxScreen";
            this.pbxScreen.Size = new System.Drawing.Size(1050, 590);
            this.pbxScreen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxScreen.TabIndex = 0;
            this.pbxScreen.TabStop = false;
            this.pbxScreen.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbxScreen_MouseDown);
            this.pbxScreen.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pbxScreen_MouseMove);
            this.pbxScreen.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbxScreen_MouseUp);
            // 
            // formView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1050, 590);
            this.Controls.Add(this.tsMove);
            this.Controls.Add(this.tsTool);
            this.Controls.Add(this.pbxScreen);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "formView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Viewing";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.formView_FormClosed);
            this.Shown += new System.EventHandler(this.formView_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.formView_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.formView_KeyUp);
            this.Resize += new System.EventHandler(this.formView_Resize);
            this.tsTool.ResumeLayout(false);
            this.tsTool.PerformLayout();
            this.tsMove.ResumeLayout(false);
            this.tsMove.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxScreen)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbxScreen;
        private System.Windows.Forms.ToolStrip tsTool;
        private System.Windows.Forms.ToolStripButton tsbtnScreenshot;
        private System.Windows.Forms.ToolStripButton tsbtnExit;
        private System.Windows.Forms.ToolStrip tsMove;
        private System.Windows.Forms.ToolStripButton tsbtnShowHide;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel tslblMove;
        private System.Windows.Forms.ToolStripButton tsbtnBlockControls;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    }
}