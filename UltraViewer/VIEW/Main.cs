﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CONTROLLER;
using System.Threading;
using System.Runtime.InteropServices;
using System.Net.NetworkInformation;

namespace UltraViewer
{
    public partial class formMain : Form
    {
        [DllImport("user32.dll")]
        private static extern int ShowWindow(IntPtr hWnd, uint Msg);
        private const uint SW_RESTORE = 0x09;

        Slave mySlave;
        public Master myMaster;
       
        Language language;
        formView _formView;

        CaptureScreen screen;

        string wallpaperPath;
        int secureDesktop;

        Roles? myRoles;

        delegate void UpdateStatusDelegate(string status, Image image, ToolStripStatusLabel ctrl);
        delegate void UpdateEnableDelegate(bool enable, Button ctrl);
        delegate void StartTimerDelegate(System.Windows.Forms.Timer tmr);

        public formMain()
        {
            InitializeComponent();

            mySlave = new Slave();
            myMaster = new Master();
            language = new Language();

            screen = new CaptureScreen();

            wallpaperPath = ChangeBackground.GetCurrentDesktopWallpaper();
            secureDesktop = (int)Microsoft.Win32.Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System", "PromptOnSecureDesktop", 1);
            Microsoft.Win32.Registry.SetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System", "PromptOnSecureDesktop", 0, Microsoft.Win32.RegistryValueKind.DWord);

            WaitingForMaster();
        }

        private void englishToolStripMenuItem_Click(object sender, EventArgs e)
        {
            language.SetLanguage((int)eLanguage.TiengAnh);
            language.ChangeLanguage(this);
        }

        private void vietnameseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            language.SetLanguage((int)eLanguage.TiengViet);
            language.ChangeLanguage(this);
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://gitlab.com/truanv/IT008.I21.UltraView");
        }

        private void UpdateStatus(string status, Image img, ToolStripStatusLabel ctrl)
        {
            if (this.InvokeRequired)
            {
                UpdateStatusDelegate dlg = new UpdateStatusDelegate(UpdateStatus);
                this.Invoke(dlg, new object[] { status, img, ctrl});
            }
            else
            {
                ctrl.Text = status;
                ctrl.Image = img;
            }
        }

        private void UpdateEnable(bool en, Button ctrl)
        {
            if (ctrl.InvokeRequired)
            {
                UpdateEnableDelegate dlg = new UpdateEnableDelegate(UpdateEnable);
                ctrl.Invoke(dlg, new object[] { en, ctrl });
            }
            else
            {
                ctrl.Enabled = en;
            }
        }

        private void StartTimer(System.Windows.Forms.Timer tmr)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate () { tmrComing.Start(); }));
            }
            else
            {
                tmr.Enabled = true;
            }
        }

        void WaitingForMaster()
        {
            UpdateEnable(false, btnStart);

            if (txbLocalIP.Text == "")
            {
                UpdateStatus("Không thể lấy IP của máy. Vui lòng kiểm tra lại!", Properties.Resources.NotConnected, tsslblConnectState);
            }

            //start listen for master
            if (mySlave.StartListen() == 1)
            {
                UpdateStatus("Đã thiết lập cấu hình mạng thành công.", Properties.Resources.Connected, tsslblConnectState);

                UpdateEnable(true, btnStart);
                StartTimer(tmrComing);
            }
            else
            {
                UpdateStatus("Không thể thiết lập cấu hình mạng.", Properties.Resources.NotConnected, tsslblConnectState);
            }
        }
		
		private void tmrComing_Tick(object sender, EventArgs e)
        {
            if (mySlave.Connected)
            {
                if (myRoles == null)
                {
                    tsslblConnectState.Text = String.Format("Phát hiện kết nối từ {0}", mySlave.GetRemoteIPAddress());
                    myRoles = Roles.Slave;
                    btnStart.Enabled = false;

                    Thread slaveThread = new Thread(Obeying);
                    slaveThread.IsBackground = true;
                    slaveThread.Start(this.Handle);

                    this.WindowState = FormWindowState.Minimized;
                }
                else
                {
                    mySlave.RefuseConnection();
                }

                tmrComing.Enabled = false;
            }
        }

        private void Obeying(object handl)
        {
            IntPtr formMainhandl = (IntPtr)handl;

            if (myRoles != Roles.Slave)
            {
                return;
            }

            //Change screen
            ChangeBackground.SetColor(Color.Black);

            //Call Obey_Thread to send burst images every 45 millisecond;
            ManualResetEvent mre = new ManualResetEvent(true);
            System.Threading.Timer myTimer = new System.Threading.Timer(Obey_Thread, mre, 0, 45);

            //This loop check if there are task to do
            while (mySlave.Connected)
            {
                if (mySlave.ReceiveVarData() > 4)
                {
                    DataConverter myConverter = new DataConverter(mySlave.Data);
                    InputSimulator.Simulate(myConverter.AutoConverter(), myConverter.Type);
                }
                Thread.Sleep(55);
            }

            ChangeBackground.SetCurrentDesktopWallpaper(wallpaperPath);

            myRoles = null;
            myTimer.Dispose();
            mySlave.Renew();
            WaitingForMaster();
            ShowWindow(formMainhandl, SW_RESTORE);
        }

        private void Obey_Thread(object stateInfo)
        {
            ManualResetEvent manualEvent = (ManualResetEvent)stateInfo;

            if (!manualEvent.WaitOne(0) || !mySlave.Connected)
            {
                //Other thread is sending screen. *I don't want to overlap byte stream.
                return;
            }

            //lock the lock
            manualEvent.Reset();

            //SEND BURST IMAGE TO MASTER
            if (screen.Capture())
            {
                mySlave.SendVarData(screen.curBuff);
            }

            //release the lock
            manualEvent.Set();
        }



        private void btnStart_Click(object sender, EventArgs e)
        {
            if (myRoles != null)
            {
                return;
            }

            myMaster.CreateSocket();

            if (PrepareToControl())
            {
                myRoles = Roles.Master;
                this.Hide();

                _formView = new formView(ref myMaster);
                _formView.ShowDialog();

                this.Show();
                myMaster.Reuse();
                myRoles = null;
            }
            txbRemoteIP.Enabled = true;
            btnStart.Enabled = true;
        }

        private bool PrepareToControl()
        {
            try
            {
                txbRemoteIP.Enabled = false;
                btnStart.Enabled = false;

                tsslblConnectState.Image = Properties.Resources.Loading;
                IPAddress remoteIP = IPAddress.Parse(txbRemoteIP.Text);
                tsslblConnectState.Text = String.Format("Đang cố gắng kết nối đến {0}", txbRemoteIP.Text);
                myMaster.StartConnect(remoteIP);

                if (myMaster.Connected)
                {
                    tsslblConnectState.Image = Properties.Resources.Connected;
                    tsslblConnectState.Text = String.Format("Bắt đầu điều khiển {0}", txbRemoteIP.Text);
                }
                else
                {
                    tsslblConnectState.Image = Properties.Resources.NotConnected;
                    tsslblConnectState.Text = "Lỗi không xác định, vui lòng liên hệ nhà phát triển.";
                    return false;
                }
                
            }
            catch (ArgumentNullException)
            {
                tsslblConnectState.Text = "Vui lòng nhập IP của máy muốn điều khiển";
                tsslblConnectState.Image = Properties.Resources.Warning;
                return false;
            }
            catch (FormatException)
            {
                tsslblConnectState.Text = "Vui lòng nhập đúng IP";
                tsslblConnectState.Image = Properties.Resources.Warning;
                return false;
            }

            return true;
        }

        private void formMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            ChangeBackground.SetCurrentDesktopWallpaper(wallpaperPath);
            Microsoft.Win32.Registry.SetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System",
                "PromptOnSecureDesktop",
                secureDesktop,
                Microsoft.Win32.RegistryValueKind.DWord);
        }

        private void formMain_Load(object sender, EventArgs e)
        {
            comboBox1.DataSource = (from adapter in NetworkInterface.GetAllNetworkInterfaces()
                                    where (adapter.NetworkInterfaceType == NetworkInterfaceType.Ethernet
                                            || adapter.NetworkInterfaceType == NetworkInterfaceType.Wireless80211)
                                            && adapter.OperationalStatus == OperationalStatus.Up
                                    select new { DisplayMember = adapter.Description, ValueMember = adapter }).ToList();
            comboBox1.DisplayMember = "DisplayMember";
            comboBox1.ValueMember = "ValueMember";
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            if (sender is ComboBox cmb)
            {
                if (cmb.SelectedValue is NetworkInterface adapter)
                {
                    var addresses = adapter.GetIPProperties().UnicastAddresses;
                    txbLocalIP.Text = (from address in addresses
                                       where address.Address.AddressFamily == AddressFamily.InterNetwork
                                       select address.Address).FirstOrDefault().ToString();
                }
            }
        }
    }
}
